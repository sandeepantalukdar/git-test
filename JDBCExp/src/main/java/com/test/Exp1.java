package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Exp1 {

	public static void main(String[] args) throws Exception
	{
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/app1", "root", "Stalukdar@98"); 
		
		/*
        insert operation
		 
		PreparedStatement pst = con.prepareStatement("insert into emp3 values(?,?,?)");
		pst.setInt(1, 103);
		pst.setString(2, "Raja");
		pst.setString(3, "Del");
		
		pst.execute();
		
		
		 Update operation
		 PreparedStatement pst = con.prepareStatement("update emp3 set emp_name=? where emp_id =?");
		
		pst.setString(1, "Ranjit");
		pst.setInt(2, 101);
	
		pst.execute();
		
		
		 //Select operation
		 PreparedStatement pst = con.prepareStatement("select * from emp3");
		 
		
		ResultSet rs = pst.executeQuery(); 
		
		while(rs.next()) {
			System.out.println("ID : "+rs.getInt(1)+" Name : "+rs.getString(2)+" City : "+rs.getString(3));
		}
		*/
		
		PreparedStatement pst = con.prepareStatement("delete from emp3 where emp_id=?");
		
		pst.setInt(1, 102);
		
		pst.execute();
		System.out.println("Done");
	}
}
